# Config Git Repository

Samples of the GitHub repository content to be read by Config Server:

##### As part of series:
- Git repo for Spring App configurations (this)
- Spring Config Server (uses these properties)
- Spring Cloud Gateway server (uses config server)
- Spring App sample application (uses config server)
- Spring Eureka Server (out of scope for this)

##### Repository content
- "content" folder - includes all components in the above context
- "content_no_eureka" folder - includes all components in the context without Eureka. Suitable for environments where load balancing and discovery are provided by the infrastructure, ex. Kubernetes

#### Usage
##### Local
Copy all the files from any of the "content" directories to a local or remote Git location, see the Spring Config server commented local file section in *application.yml*
##### Remote
Link to this repository and use the "content" directory as a search path, see the Spring Config server section in *application.yml*
